import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {
    public static final String SPACE_PATTERN = "\\s+";

    public String getResult(String sentence) {
        if (sentence.split(SPACE_PATTERN).length == 1) {
            return sentence + " 1";
        }
        List<Input> inputList = countWords(sentence);
        return inputList.stream()
                .sorted(Comparator.comparingInt(Input::getWordCount).reversed())
                .map(Input::toString)
                .collect(Collectors.joining("\n"));

    }

    private List<Input> countWords(String sentence) {
        List<String> words = List.of(sentence.split(SPACE_PATTERN));
        HashSet<String> deduplicate = new HashSet<>(words);
        return deduplicate.stream()
                .map(word -> new Input(word, Collections.frequency(words, word)))
                .collect(Collectors.toList());
    }
}
